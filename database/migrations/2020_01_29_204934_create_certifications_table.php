<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('certification_type_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('certified_by');
            $table->string('document_path')->nullable();
            $table->date('certification_date');
            $table->date('expiry_date');
            $table->integer('status')->unsigned();
            $table->bigInteger('renewal_for_id')->unsigned()->nullable();
            $table->bigInteger('recertification_for_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
