<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentCalibrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('equipment_calibrations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('equipment_id');
            $table->string('calibrated_by');
            $table->string('document_path')->nullable();
            $table->integer('status')->unsigned();
            $table->date('calibration_date');
            $table->date('expiry_date');
            $table->bigInteger('renewal_for_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('equipment_calibrations');
    }
}
