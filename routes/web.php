<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register'=>false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth']], function()
{
    //users
    Route::resource('users', 'UserController');

    //certification types
    Route::resource('certification_types', 'CertificationTypeController');

    //certifications
    Route::group(['prefix'=>'certifications'], function(){
        Route::get('/expiring_index', 'CertificationController@expiringIndex')->name('certifications.expiring_index');
        Route::get('/download-document/{certification}', 'CertificationController@downloadDocument')->name('certifications.download_document');
    });
    Route::resource('certifications', 'CertificationController');

    //eye tests
    Route::group(['prefix'=>'eye_tests'], function(){
        Route::get('/expiring_index', 'EyeTestController@expiringIndex')->name('eye_tests.expiring_index');
        Route::get('/download-document/{eye_test}', 'EyeTestController@downloadDocument')->name('eye_tests.download_document');
    });
    Route::resource('eye_tests', 'EyeTestController');

    //equipment
    Route::resource('equipment', 'EquipmentController');

    // equipment calibrations
    Route::group(['prefix'=>'equipment_calibrations'], function(){
        Route::get('/download-document/{equipment_calibration}', 'EquipmentCalibrationController@downloadDocument')->name('equipment_calibrations.download_document');
        Route::get('/expiring_index', 'EquipmentCalibrationController@expiringIndex')->name('equipment_calibrations.expiring_index');
    });
    Route::resource('equipment_calibrations', 'EquipmentCalibrationController');

    // equipment users
    Route::group(['prefix'=>'equipment_users'], function(){
        Route::get('/return/{equipment_user}', 'EquipmentUserController@return')->name('equipment_users.return');
    });
    Route::resource('equipment_users', 'EquipmentUserController');
});
