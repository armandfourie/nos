@extends('layouts.app')

@section('content')

<h1>Equipment - {{$equipment->type}} - {{$equipment->asset_number}}</h1>

<br>

<!-- <div class="col-8"> -->
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-details-tab" data-toggle="tab" href="#nav-details" role="tab" aria-controls="nav-details" aria-selected="true">Equipment Details</a>
            <a class="nav-item nav-link" id="nav-calibrations-tab" data-toggle="tab" href="#nav-calibrations" role="tab" aria-controls="nav-calibrations" aria-selected="false">Calibrations</a>
            <a class="nav-item nav-link" id="nav-bookings-tab" data-toggle="tab" href="#nav-bookings" role="tab" aria-controls="nav-bookings" aria-selected="false">Bookings</a>
        </div>
    </nav>

    <div class="tab-content" id="nav-tabContent">

        <!-- DETAILS NAV -->
        <div class="tab-pane fade show active" id="nav-details" role="tabpanel" aria-labelledby="nav-details-tab">
            <br>
            {!! Form::model($equipment, ['method'=>'PATCH', 'action'=>['EquipmentController@update', $equipment], 'files'=>false]) !!}

                <div class='form-group'>
                    {!! Form::label('asset_number', 'Asset Number:') !!}
                    {!! Form::text('asset_number', null, ['class'=>'form-control']) !!}
                    @error('asset_number')
                        <p class='text-danger'>{{$message}}</p>
                    @enderror
                </div>

                <div class='form-group'>
                    {!! Form::label('type', 'Type:') !!}
                    {!! Form::text('type', null, ['class'=>'form-control']) !!}
                    @error('type')
                        <p class='text-danger'>{{$message}}</p>
                    @enderror
                </div>

                <div class='form-group'>
                    {!! Form::label('manufacturer', 'Manufacturer:') !!}
                    {!! Form::text('manufacturer', null, ['class'=>'form-control']) !!}
                    @error('manufacturer')
                        <p class='text-danger'>{{$message}}</p>
                    @enderror
                </div>

                <div class='form-group'>
                    {!! Form::label('model', 'Model:') !!}
                    {!! Form::text('model', null, ['class'=>'form-control']) !!}
                    @error('model')
                        <p class='text-danger'>{{$message}}</p>
                    @enderror
                </div>

                <div class='form-group'>
                    {!! Form::label('serial_number', 'Serial Number:') !!}
                    {!! Form::text('serial_number', null, ['class'=>'form-control']) !!}
                    @error('serial_number')
                        <p class='text-danger'>{{$message}}</p>
                    @enderror
                </div>

                {!! Form::submit('Save Changes', ['class'=>'btn btn-secondary']) !!}

            {!! Form::close() !!}

        </div>

        <!-- CALIBRATIONS NAV -->
        <div class="tab-pane fade" id="nav-calibrations" role="tabpanel" aria-labelledby="nav-calibrations-tab">

            <br>

            <!-- Add Calibration Form -->

            {!! Form::open(['method'=>'GET', 'action'=>['EquipmentCalibrationController@create'], 'files'=>false]) !!}

                {!! Form::hidden('equipment_id', $equipment->id) !!}

                {!! Form::submit('Add Calibration', ['class'=>'btn btn-primary']) !!}

            {!! Form::close() !!}

            <br>

            <table class="table tabel-sm table-hover">

                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Calibrated By</th>
                        <th scope="col"></th>
                        <th scope="col">Status</th>
                        <th scope="col">Calibration Date</th>
                        <th scope="col">Expiry Date</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($equipment_calibrations) > 0)
                        @foreach ($equipment_calibrations as $equipment_calibration)
                        <tr>
                            <th scope="row">{{$equipment_calibration->id}}</th>
                            <td>{{$equipment_calibration->calibrated_by}}</td>
                            <td><a href="{{route('equipment_calibrations.download_document', $equipment_calibration)}}">{{$equipment_calibration->document_path ? 'doc' : ''}}</a></td>
                            <td>{{$equipment_calibration->read_status}}</td>
                            <td>{{$equipment_calibration->calibration_date}}</td>
                            <td>{{$equipment_calibration->expiry_date}}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">

                                    @if($equipment_calibration->requiresRenewal())

                                        {!! Form::open(['method'=>'GET', 'action'=>['EquipmentCalibrationController@create'], 'files'=>false]) !!}

                                            {!! Form::hidden('equipment_calibration_id', $equipment_calibration->id) !!}
                                            {!! Form::hidden('equipment_id', $equipment_calibration->equipment_id) !!}

                                            {!! Form::submit('Renew', ['class'=>'btn btn-sm btn-success']) !!}

                                        {!! Form::close() !!}

                                    @endif

                                    @can('update', $equipment_calibration)
                                        {!! Form::open(['method'=>'GET', 'action'=>['EquipmentCalibrationController@edit', $equipment_calibration], 'files'=>false]) !!}

                                            {!! Form::submit('Edit', ['class'=>'btn btn-sm btn-secondary']) !!}

                                        {!! Form::close() !!}
                                    @endcan

                                    @can('delete', $equipment_calibration)
                                        {!! Form::open(['method'=>'DELETE', 'action'=>['EquipmentCalibrationController@destroy', $equipment_calibration], 'files'=>false]) !!}

                                            {!! Form::submit('Delete', ['class'=>'btn btn-sm btn-danger']) !!}

                                        {!! Form::close() !!}
                                    @endcan

                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <th></th>
                            <td colspan="5">Equipment has not been calibrated</td>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>

        <!-- BOOKINGS NAV -->
        <div class="tab-pane fade" id="nav-bookings" role="tabpanel" aria-labelledby="nav-bookings-tab">

            <br>

            <!-- Add Booking Form -->

            @if($equipment->isCalibrated() && $equipment->isNotCheckedOut())

                <p>Equipment available for checkout</p>


                {!! Form::open(['method'=>'GET', 'action'=>['EquipmentUserController@create'], 'files'=>false]) !!}

                    {!! Form::hidden('equipment_id', $equipment->id) !!}

                    {!! Form::submit('Checkout', ['class'=>'btn btn-primary']) !!}

                {!! Form::close() !!}

            @else

                <p>Equipment currently unavailable</p>

            @endif

            <br>

            <table class="table tabel-sm table-hover">

                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Booked By</th>
                        <th scope="col">Issue Date</th>
                        <th scope="col">Planned Return Date</th>
                        <th scope="col">Return Date</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($equipment_users) > 0)
                        @foreach ($equipment_users as $equipment_user)
                        <tr>
                            <th scope="row">{{$equipment_user->pivot->id}}</th>
                            <td>{{$equipment_user->name}}</td>
                            <td>{{$equipment_user->pivot->issue_date}}</td>
                            <td>{{$equipment_user->pivot->planned_return_date}}</td>
                            <td>{{$equipment_user->pivot->return_date}}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group" aria-label="...">

                                    @if($equipment_user->pivot->return_date == null)

                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#returnModal-{{$equipment_user->pivot->id}}">
                                            Return
                                        </button>

                                        <!-- Modal -->
                                        <div class="modal fade" id="returnModal-{{$equipment_user->pivot->id}}" tabindex="-1" role="dialog" aria-labelledby="returnModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Return - {{$equipment->type}} - {{$equipment->asset_number}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! Form::open(['method'=>'GET', 'action'=>['EquipmentUserController@return', $equipment_user->pivot->id], 'files'=>false]) !!}

                                                            <div class='form-group'>
                                                                <div class='form-group'>
                                                                    {!! Form::label('return_date', 'Date Returned:') !!}
                                                                    {!! Form::date('return_date', null, ['class'=>'form-control']) !!}
                                                                    @error('return_date')
                                                                        <p class='text-danger'>{{$message}}</p>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                            {!! Form::submit('Return', ['class'=>'btn btn-sm btn-success']) !!}

                                                        {!! Form::close() !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endif

                                    @can('update', $equipment_user)
                                        {!! Form::open(['method'=>'GET', 'action'=>['EquipmentUserController@edit', $equipment_user->pivot->id], 'files'=>false]) !!}

                                            {!! Form::submit('Edit', ['class'=>'btn btn-sm btn-secondary']) !!}

                                        {!! Form::close() !!}
                                    @endcan

                                    @can('delete', $equipment_user)
                                        {!! Form::open(['method'=>'DELETE', 'action'=>['EquipmentUserController@destroy', $equipment_user->pivot->id], 'files'=>false]) !!}

                                            {!! Form::submit('Delete', ['class'=>'btn btn-sm btn-danger']) !!}

                                        {!! Form::close() !!}
                                    @endcan

                                </div>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <th></th>
                            <td colspan="5">Equipment has no bookings</td>
                        </tr>
                    @endif
                </tbody>
            </table>

        </div>

    </div>

<!-- </div> -->

@stop
