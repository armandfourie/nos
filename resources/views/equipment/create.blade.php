@extends('layouts.app')

@section('content')

<h1>Register Equipment</h1>

{!! Form::open(['method'=>'POST', 'action'=>['EquipmentController@store'], 'files'=>false]) !!}

    <div class='form-group'>
        {!! Form::label('asset_number', 'Asset Number:') !!}
        {!! Form::text('asset_number', null, ['class'=>'form-control']) !!}
        @error('asset_number')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('type', 'Type:') !!}
        {!! Form::text('type', null, ['class'=>'form-control']) !!}
        @error('type')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('manufacturer', 'Manufacturer:') !!}
        {!! Form::text('manufacturer', null, ['class'=>'form-control']) !!}
        @error('manufacturer')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('model', 'Model:') !!}
        {!! Form::text('model', null, ['class'=>'form-control']) !!}
        @error('model')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('serial_number', 'Serial Number:') !!}
        {!! Form::text('serial_number', null, ['class'=>'form-control']) !!}
        @error('serial_number')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Register', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}

@stop
