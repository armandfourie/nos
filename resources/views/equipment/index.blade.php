@extends('layouts.app')

@section('content')

<h1>Equipment Register</h1>

<br>

<a href="{{route('equipment.create')}}" class="btn btn-sm btn-primary">Register Equipment</a>

<br><br>


<table class="table tabel-sm table-hover">

    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Asset Number</th>
            <th scope="col">Type</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">Serial Number</th>
            <th scope="col">Calibration Status</th>
            <th scope="col">Expiry Date</th>
            <th></th>
            <th scope="col">Actions</th>
        </tr>
    </thead>

    <tbody>
        @if (count($equipment) > 0)

            @foreach ($equipment as $equipment)
                <tr>
                    <th scope="row">{{$equipment->id}}</th>
                    <td>{{$equipment->asset_number}}</td>
                    <td>{{$equipment->type}}</td>
                    <td>{{$equipment->manufacturer}}</td>
                    <td>{{$equipment->model}}</td>
                    <td>{{$equipment->serial_number}}</td>
                    @if (count($equipment->calibrations) > 0)
                        <td>{{$equipment->calibrations()->orderBy('expiry_date', 'desc')->first()->read_status}}</td>
                        <td>{{$equipment->calibrations()->orderBy('expiry_date', 'desc')->first()->expiry_date}}</td>
                        <td><a href="{{route('equipment_calibrations.download_document', $equipment->calibrations()->orderBy('expiry_date', 'desc')->first())}}">{{$equipment->calibrations()->orderBy('expiry_date', 'desc')->first()->document_path ? 'doc' : ''}}</a></td>
                    @else
                        <td colspan="3">No calibration records</td>
                    @endif
                    <td>
                        <div class="btn-group btn-group-sm" role="group" aria-label="...">

                            @can('view', $equipment)
                                <a href="{{route('equipment.show', $equipment)}}" class="btn btn-sm btn-info">Details</a>
                            @endcan
                            @can('update', $equipment)
                                {!! Form::open(['method'=>'GET', 'action'=>['EquipmentController@edit', $equipment], 'files'=>false]) !!}

                                    {!! Form::submit('Edit', ['class'=>'btn btn-sm btn-secondary']) !!}

                                {!! Form::close() !!}
                            @endcan
                            @can('delete', $equipment)
                                {!! Form::open(['method'=>'DELETE', 'action'=>['EquipmentController@destroy', $equipment], 'files'=>false]) !!}

                                    {!! Form::submit('Delete', ['class'=>'btn btn-sm btn-danger']) !!}

                                {!! Form::close() !!}
                            @endcan

                        </div>
                    </td>
                </tr>
            @endforeach

        @else
            <tr>
                <th></th>
                <td colspan="5">There are no equipment registered</td>
            </tr>
        @endif
  </tbody>
</table>

@stop
