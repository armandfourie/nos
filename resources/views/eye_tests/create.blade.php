@extends('layouts.app')

@section('content')

<h1>Create Eye for {{$user->name}}</h1>

{!! Form::open(['method'=>'POST', 'action'=>['EyeTestController@store'], 'files'=>true]) !!}

    {!! Form::hidden('user_id', $user->id) !!}

    @if ($eye_test)

        {!! Form::hidden('eye_test_id', $eye_test->id) !!}

    @endif

    <div class='form-group'>
        {!! Form::label('certified_by', 'Certified By:') !!}
        {!! Form::text('certified_by', null, ['class'=>'form-control']) !!}
        @error('certified_by')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('file', 'Add File:') !!}<br>
        {!! Form::file('file') !!}
        @error('file')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('test_date', 'Test Date:') !!}
        {!! Form::date('test_date', null, ['class'=>'form-control']) !!}
        @error('test_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Create Eye Test', ['class'=>'btn btn-primary']) !!}


{!! Form::close() !!}

@endsection
