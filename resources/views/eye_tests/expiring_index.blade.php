@extends('layouts.app')

@section('content')

<h1>Expiring Eye Tests</h1>

<br>

<table class="table tabel-sm table-hover">

    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">User</th>
            <th scope="col">Status</th>
            <th scope="col">Test Date</th>
            <th scope="col">Expiry Date</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>

    <tbody>
        @if (count($expiring_eye_tests) > 0)

            @foreach ($expiring_eye_tests as $eye_test)
                <tr>
                    <th scope="row">{{$eye_test->id}}</th>
                    <td>{{$eye_test->user->name}}</td>
                    <td>{{$eye_test->read_status}}</td>
                    <td>{{$eye_test->test_date}}</td>
                    <td>{{$eye_test->expiry_date}}</td>
                    <td>
                        {!! Form::open(['method'=>'GET', 'action'=>['EyeTestController@create'], 'files'=>false]) !!}

                            {!! Form::hidden('user_id', $eye_test->user->id) !!}

                            {!! Form::hidden('eye_test_id', $eye_test->id) !!}

                            {!! Form::submit('Renew', ['class'=>'btn btn-sm btn-success']) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach

        @else
            <tr>
                <th></th>
                <td colspan="5">There are no eye tests requiring renewal</td>
            </tr>
        @endif
  </tbody>
</table>

@stop
