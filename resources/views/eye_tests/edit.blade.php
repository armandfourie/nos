@extends('layouts.app')

@section('content')

<h1>Edit Eye Test</h1>

{!! Form::model($eye_test, ['method'=>'PATCH', 'action'=>['EyeTestController@update', $eye_test], 'files'=>true]) !!}

    <div class='form-group'>
        {!! Form::label('certified_by', 'Certified By:') !!}
        {!! Form::text('certified_by', null, ['class'=>'form-control']) !!}
        @error('certified_by')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('file', 'Add File:') !!}<br>
        {!! Form::file('file') !!}
        @error('file')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('test_date', 'Test Date:') !!}
        {!! Form::date('test_date', null, ['class'=>'form-control']) !!}
        @error('test_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Update', ['class'=>'btn btn-secondary']) !!}


{!! Form::close() !!}

@endsection
