@extends('layouts.app')

@section('content')

    <h1>Update certification for {{$certification->user->name}}</h1>

    {!! Form::model($certification, ['method'=>'PATCH', 'action'=>['CertificationController@update', $certification], 'files'=>true]) !!}

        <div class='form-group'>
            {!! Form::label('certification_type_id', 'Certification Type:') !!}
            {!! Form::select('certification_type_id', [$certification->certificationType->id => $certification->code], null, ['class'=>'form-control', 'readonly'=>'true']) !!}
            @error('certification_type_id')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('certified_by', 'Certified By:') !!}
            {!! Form::text('certified_by', null, ['class'=>'form-control']) !!}
            @error('certified_by')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('file', 'Add File:') !!}<br>
            {!! Form::file('file') !!}
            @error('file')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('certification_date', 'Certification Date:') !!}
            {!! Form::date('certification_date', null, ['class'=>'form-control']) !!}
            @error('certification_date')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        {!! Form::submit('Save Changes', ['class'=>'btn btn-secondary']) !!}

    {!! Form::close() !!}

@stop
