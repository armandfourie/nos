@extends('layouts.app')

@section('content')

    <h1>Expiring Certifications</h1>

    <!-- EXPIRING CERTFICATIONS NAV -->
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-renewal-tab" data-toggle="tab" href="#nav-renewal" role="tab" aria-controls="nav-renewal" aria-selected="false">Renewals</a>
            <a class="nav-item nav-link" id="nav-recertifications-tab" data-toggle="tab" href="#nav-recertifications" role="tab" aria-controls="nav-recertifications" aria-selected="false">Re-Certifications</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <!-- RENEWALS -->
        <div class="tab-pane fade show active" id="nav-renewal" role="tabpanel" aria-labelledby="nav-renewal-tab">

            <br>

            <table class="table tabel-sm table-hover">

                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Code</th>
                        <th scope="col">User</th>
                        <th scope="col">Status</th>
                        <th scope="col">Certification Date</th>
                        <th scope="col">Expiry Date</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($certifications_renewal) > 0)

                        @foreach ($certifications_renewal as $certification)
                            <tr>
                                <th scope="row">{{$certification->id}}</th>
                                <td><a href="{{route('certification_types.show', $certification->certificationType)}}">{{$certification->code}}</a></td>
                                <td>{{$certification->user->name}}</td>
                                <td>{{$certification->read_status}}</td>
                                <td>{{$certification->certification_date}}</td>
                                <td>{{$certification->expiry_date}}</td>
                                <td>
                                    {!! Form::open(['method'=>'GET', 'action'=>['CertificationController@create'], 'files'=>false]) !!}

                                        {!! Form::hidden('user_id', $certification->user->id) !!}

                                        {!! Form::hidden('certification_id', $certification->id) !!}

                                        {!! Form::submit('Renew', ['class'=>'btn btn-sm btn-success']) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    @else
                        <tr>
                            <th></th>
                            <td colspan="5">There are no certifications requiring renewal</td>
                        </tr>
                    @endif
              </tbody>
            </table>

        </div>
        
        <!-- RE-CERTIFICATIONS -->
        <div class="tab-pane fade" id="nav-recertifications" role="tabpanel" aria-labelledby="nav-recertifications-tab">

            <br>

            <table class="table tabel-sm table-hover">

                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Code</th>
                        <th scope="col">User</th>
                        <th scope="col">Status</th>
                        <th scope="col">Certification Date</th>
                        <th scope="col">Expiry Date</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>

                <tbody>
                    @if (count($certifications_recertification) > 0)

                        @foreach ($certifications_recertification as $certification)
                            <tr>
                                <th scope="row">{{$certification->id}}</th>
                                <td><a href="{{route('certification_types.show', $certification->certificationType)}}">{{$certification->code}}</a></td>
                                <td>{{$certification->user->name}}</td>
                                <td>{{$certification->read_status}}</td>
                                <td>{{$certification->certification_date}}</td>
                                <td>{{$certification->expiry_date}}</td>
                                <td>
                                    {!! Form::open(['method'=>'GET', 'action'=>['CertificationController@create'], 'files'=>false]) !!}

                                        {!! Form::hidden('user_id', $certification->user->id) !!}

                                        {!! Form::hidden('certification_id', $certification->id) !!}

                                        {!! Form::submit('Re-Certify', ['class'=>'btn btn-sm btn-success']) !!}

                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach

                    @else
                        <tr>
                            <th></th>
                            <td colspan="5">There are no certifications requiring re-certification</td>
                        </tr>
                    @endif
              </tbody>
            </table>

        </div>
    </div>


@endsection
