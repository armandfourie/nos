@extends('layouts.app')

@section('content')

    <h1>Add certification for {{$user->name}}</h1>

    <br>

    {!! Form::open(['method'=>'POST', 'action'=>['CertificationController@store'], 'files'=>true]) !!}

    {!! Form::hidden('user_id', $user->id) !!}

        @if ($certification)

            {!! Form::hidden('certification_id', $certification->id) !!}

            <div class='form-group'>
                {!! Form::label('certification_type_id', 'Certification Type:') !!}
                {!! Form::select('certification_type_id', [$certification->certificationType->id =>$certification_types[$certification->certificationType->id]], null, ['class'=>'form-control', 'readonly'=>true]) !!}
                @error('certification_type_id')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>
        @else
            <div class='form-group'>
                {!! Form::label('certification_type_id', 'Certification Type:') !!}
                {!! Form::select('certification_type_id', [''=>'Select Certification'] + $certification_types, null, ['class'=>'form-control']) !!}
                @error('certification_type_id')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>
        @endif

        <div class='form-group'>
            {!! Form::label('certified_by', 'Certified By:') !!}
            {!! Form::text('certified_by', null, ['class'=>'form-control']) !!}
            @error('certified_by')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('file', 'Add File:') !!}<br>
            {!! Form::file('file') !!}
            @error('file')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('certification_date', 'Certification Date:') !!}
            {!! Form::date('certification_date', null, ['class'=>'form-control']) !!}
            @error('certification_date')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        {!! Form::submit('Add Certificate', ['class'=>'btn btn-primary']) !!}

    {!! Form::close() !!}

@stop
