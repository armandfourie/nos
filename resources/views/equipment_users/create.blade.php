@extends('layouts.app')

@section('content')

<h1>Checking Out {{$equipment->type}} - {{$equipment->asset_number}}</h1>

{!! Form::open(['method'=>'POST', 'action'=>['EquipmentUserController@store'], 'files'=>false]) !!}

    {!! Form::hidden('equipment_id', $equipment->id) !!}

    <div class='form-group'>
        {!! Form::label('user_id', 'User:') !!}
        {!! Form::select('user_id', [''=>'Select Users'] + $users, null, ['class'=>'form-control']) !!}
        @error('user_id')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('issue_date', 'Checkout Date:') !!}
        {!! Form::date('issue_date', null, ['class'=>'form-control']) !!}
        @error('issue_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('planned_return_date', 'Planned Return Date:') !!}
        {!! Form::date('planned_return_date', null, ['class'=>'form-control']) !!}
        @error('planned_return_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('comments', 'Comments:') !!}
        {!! Form::textarea('comments', null, ['class'=>'form-control', 'rows'=>3]) !!}
        @error('comments')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Checkout', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}

@stop
