@extends('layouts.app')

@section('content')

<h1>Add Calibration For {{$equipment->type}} - {{$equipment->asset_number}}</h1>

{!! Form::open(['method'=>'POST', 'action'=>['EquipmentCalibrationController@store'], 'files'=>true]) !!}

    {!! Form::hidden('equipment_id', $equipment->id) !!}

    @if ($equipment_calibration)

        {!! Form::hidden('equipment_calibration_id', $equipment_calibration->id) !!}

    @endif

    <div class='form-group'>
        {!! Form::label('calibrated_by', 'Calibrated By:') !!}
        {!! Form::text('calibrated_by', null, ['class'=>'form-control']) !!}
        @error('calibrated_by')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('file', 'Add File:') !!}<br>
        {!! Form::file('file') !!}
        @error('file')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('calibration_date', 'Calibration Date:') !!}
        {!! Form::date('calibration_date', null, ['class'=>'form-control']) !!}
        @error('calibration_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('expiry_date', 'Expiry Date:') !!}
        {!! Form::date('expiry_date', null, ['class'=>'form-control']) !!}
        @error('expiry_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Add Calibration', ['class'=>'btn btn-primary']) !!}

{!! Form::close() !!}

@stop
