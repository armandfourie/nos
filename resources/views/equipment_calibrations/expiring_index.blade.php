@extends('layouts.app')

@section('content')

<h1>Expiring Equipment Calibrations</h1>

<br>

<table class="table tabel-sm table-hover">

    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Asset Number</th>
            <th scope="col">Type</th>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">Serial Number</th>
            <th scope="col">Calibration Status</th>
            <th scope="col">Expiry Date</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>

    <tbody>
        @if (count($equipment_calibrations) > 0)

            @foreach ($equipment_calibrations as $equipment_calibration)
                <tr>
                    <th scope="row">{{$equipment_calibration->id}}</th>
                    <td><a href="{{route('equipment.show', $equipment_calibration->equipment)}}">{{$equipment_calibration->equipment->asset_number}}</a></td>
                    <td>{{$equipment_calibration->equipment->type}}</td>
                    <td>{{$equipment_calibration->equipment->manufacturer}}</td>
                    <td>{{$equipment_calibration->equipment->model}}</td>
                    <td>{{$equipment_calibration->equipment->serial_number}}</td>
                    <td>{{$equipment_calibration->read_status}}</td>
                    <td>{{$equipment_calibration->expiry_date}}</td>
                    <td>
                        <div class="btn-group btn-group-sm" role="group" aria-label="...">

                            @can('update', $equipment_calibration)
                                {!! Form::open(['method'=>'GET', 'action'=>['EquipmentCalibrationController@create'], 'files'=>false]) !!}

                                    {!! Form::hidden('equipment_id', $equipment_calibration->equipment_id) !!}

                                    {!! Form::hidden('equipment_calibration_id', $equipment_calibration->id) !!}

                                    {!! Form::submit('Renew', ['class'=>'btn btn-sm btn-success']) !!}

                                {!! Form::close() !!}
                            @endcan

                        </div>
                    </td>
                </tr>
            @endforeach

        @else
            <tr>
                <th></th>
                <td colspan="5">There are no equipment calibrations that are expiring</td>
            </tr>
        @endif
  </tbody>
</table>

@stop
