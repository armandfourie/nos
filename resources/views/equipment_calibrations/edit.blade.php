@extends('layouts.app')

@section('content')

<h1>Add Calibration For {{$equipment_calibration->equipment->type}} - {{$equipment_calibration->equipment->asset_number}}</h1>

{!! Form::model($equipment_calibration, ['method'=>'PATCH', 'action'=>['EquipmentCalibrationController@update', $equipment_calibration], 'files'=>true]) !!}

    <div class='form-group'>
        {!! Form::label('calibrated_by', 'Calibrated By:') !!}
        {!! Form::text('calibrated_by', null, ['class'=>'form-control']) !!}
        @error('calibrated_by')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('file', 'Add File:') !!}<br>
        {!! Form::file('file') !!}
        @error('file')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('calibration_date', 'Calibration Date:') !!}
        {!! Form::date('calibration_date', null, ['class'=>'form-control']) !!}
        @error('calibration_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('expiry_date', 'Expiry Date:') !!}
        {!! Form::date('expiry_date', null, ['class'=>'form-control']) !!}
        @error('expiry_date')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::submit('Save Changes', ['class'=>'btn btn-secondary']) !!}

{!! Form::close() !!}

@stop
