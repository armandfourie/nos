@extends('layouts.app')

@section('content')

<h1>Manage Profile</h1>

{!! Form::model($user, ['method'=>'PATCH', 'action'=>['UserController@update', $user], 'files'=>false]) !!}

    <div class='form-group'>
        {!! Form::label('employee_no', 'Employee Number:') !!}
        {!! Form::text('employee_no', null, ['class'=>'form-control']) !!}
        @error('employee_no')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @error('name')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
        @error('email')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('mobile', 'Mobile:') !!}
        {!! Form::text('mobile', null, ['class'=>'form-control']) !!}
        @error('mobile')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('identity', 'ID Number:') !!}
        {!! Form::text('identity', null, ['class'=>'form-control']) !!}
        @error('identity')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::label('Address:') !!}
    <div class='form-group'>
        {!! Form::text('street_number', $address ? $address->street_number : null, ['class'=>'form-control', 'placeholder'=>'Street Number']) !!}
        @error('street_number')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('suburb', $address ? $address->suburb : null, ['class'=>'form-control', 'placeholder'=>'Suburb']) !!}
        @error('suburb')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('city', $address ? $address->city : null, ['class'=>'form-control', 'placeholder'=>'City']) !!}
        @error('city')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('province', $address ? $address->province : null, ['class'=>'form-control', 'placeholder'=>'Province']) !!}
        @error('province')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('postal_code', $address ? $address->postal_code : null, ['class'=>'form-control', 'placeholder'=>'Postal Code']) !!}
        @error('postal_code')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <br>

    {!! Form::submit('Save Changes', ['class'=>'btn btn-secondary']) !!}

{!! Form::close() !!}


@stop
