@extends('layouts.app')

@section('content')

    <!-- FILTER SECTION -->
    <div class="row border border-info rounded">

        <div class="col">
            <br>
            {!! Form::open(['method'=>'GET', 'action'=>['UserController@index'], 'files'=>false, 'class'=>'form-inline']) !!}

                <div class='form-group'>
                    {!! Form::label('id', 'Select User:') !!}
                    {!! Form::select('id', [''=>'Name'] + $filter_users, null, ['class'=>'form-control mx-sm-3']) !!}
                </div>

                {!! Form::submit('Filter', ['class'=>'btn btn-primary']) !!}

            {!! Form::close() !!}
            <br>
        </div>

    </div>
    <br>
    <!-- MAIN BODY -->
    <div class="row justify-content-between">

        <!-- USER INFO NAV -->
        <div class="col-8">
            <h1>{{$user->name}}</h1>
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-manage-tab" data-toggle="tab" href="#nav-manage" role="tab" aria-controls="nav-manage" aria-selected="true">User Info</a>
                    <a class="nav-item nav-link" id="nav-certifications-tab" data-toggle="tab" href="#nav-certifications" role="tab" aria-controls="nav-certifications" aria-selected="false">View Certifications</a>
                    <a class="nav-item nav-link" id="nav-eye_tests-tab" data-toggle="tab" href="#nav-eye_tests" role="tab" aria-controls="nav-eye_tests" aria-selected="false">Eye Tests</a>
                </div>
            </nav>

            <div class="tab-content" id="nav-tabContent">

                <!-- MANAGE PROFILE NAV -->
                <div class="tab-pane fade show active" id="nav-manage" role="tabpanel" aria-labelledby="nav-manage-tab">
                    <br>
                    {!! Form::model($user, ['method'=>'PATCH', 'action'=>['UserController@update', $user], 'files'=>false]) !!}

                        <div class='form-group'>
                            {!! Form::label('employee_no', 'Employee Number:') !!}
                            {!! Form::text('employee_no', null, ['class'=>'form-control']) !!}
                            @error('employee_no')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        <div class='form-group'>
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}
                            @error('name')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        <div class='form-group'>
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::text('email', null, ['class'=>'form-control']) !!}
                            @error('email')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        <div class='form-group'>
                            {!! Form::label('mobile', 'Mobile:') !!}
                            {!! Form::text('mobile', null, ['class'=>'form-control']) !!}
                            @error('mobile')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        <div class='form-group'>
                            {!! Form::label('identity', 'ID Number:') !!}
                            {!! Form::text('identity', null, ['class'=>'form-control']) !!}
                            @error('identity')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        {!! Form::label('Address:') !!}
                        <div class='form-group'>
                            {!! Form::text('street_number', $address ? $address->street_number : null, ['class'=>'form-control', 'placeholder'=>'Street Number']) !!}
                            @error('street_number')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>
                        <div class='form-group'>
                            {!! Form::text('suburb', $address ? $address->suburb : null, ['class'=>'form-control', 'placeholder'=>'Suburb']) !!}
                            @error('suburb')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>
                        <div class='form-group'>
                            {!! Form::text('city', $address ? $address->city : null, ['class'=>'form-control', 'placeholder'=>'City']) !!}
                            @error('city')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>
                        <div class='form-group'>
                            {!! Form::text('province', $address ? $address->province : null, ['class'=>'form-control', 'placeholder'=>'Province']) !!}
                            @error('province')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>
                        <div class='form-group'>
                            {!! Form::text('postal_code', $address ? $address->postal_code : null, ['class'=>'form-control', 'placeholder'=>'Postal Code']) !!}
                            @error('postal_code')
                                <p class='text-danger'>{{$message}}</p>
                            @enderror
                        </div>

                        <br>

                        {!! Form::label('Admin User:') !!}
                        <div class='form-check'>
                            {!! Form::radio('is_admin', '1', ['class'=>'form-check-input']) !!}
                            {!! Form::label('is_admin', 'Admin') !!}
                        </div>
                        <div class='form-check'>
                            {!! Form::radio('is_admin', '0', ['class'=>'form-check-input']) !!}
                            {!! Form::label('is_admin', 'Normal Personnel') !!}
                        </div>

                        <br>

                        {!! Form::submit('Save Changes', ['class'=>'btn btn-secondary']) !!}

                    {!! Form::close() !!}

                </div>

                <!-- CERTIFICATIONS NAV -->
                <div class="tab-pane fade" id="nav-certifications" role="tabpanel" aria-labelledby="nav-certifications-tab">

                    <br>

                    <!-- Add Certifications Form -->

                    {!! Form::open(['method'=>'GET', 'action'=>['CertificationController@create'], 'files'=>false]) !!}

                        {!! Form::hidden('user_id', $user->id) !!}

                        {!! Form::submit('Add Certification', ['class'=>'btn btn-primary']) !!}

                    {!! Form::close() !!}

                    <br>

                    <table class="table tabel-sm table-hover">

                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Code</th>
                                <th scope="col">Certified By</th>
                                <th scope="col">Document</th>
                                <th scope="col">Status</th>
                                <th scope="col">Certification Date</th>
                                <th scope="col">Expiry Date</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if (count($certifications) > 0)

                                @foreach ($certifications as $certification)
                                    <tr>
                                        <th scope="row">{{$certification->id}}</th>
                                        <td><a href="{{route('certification_types.show', $certification->certificationType)}}">{{$certification->code}}</a></td>
                                        <td>{{$certification->certified_by}}</td>
                                        <td><a href="{{route('certifications.download_document', $certification)}}">{{$certification->document_path ? 'doc' : ''}}</a></td>
                                        <td>{{$certification->read_status}}</td>
                                        <td>{{$certification->certification_date}}</td>
                                        <td>{{$certification->expiry_date}}</td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                                <!-- @can('create', $certification)
                                                    {!! Form::open(['method'=>'GET', 'action'=>['CertificationController@create'], 'files'=>false]) !!}

                                                        {!! Form::hidden('user_id', $user->id) !!}

                                                        {!! Form::hidden('certification_id', $certification->id) !!}

                                                        {!! Form::submit('Renew', ['class'=>'btn btn-success btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan -->
                                                @can('update', $certification)
                                                    {!! Form::open(['method'=>'GET', 'action'=>['CertificationController@edit', $certification], 'files'=>false]) !!}

                                                        {!! Form::submit('Edit', ['class'=>'btn btn-secondary btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan
                                                @can('delete', $certification)
                                                    {!! Form::open(['method'=>'DELETE', 'action'=>['CertificationController@destroy', $certification], 'files'=>false]) !!}

                                                        {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                            @else
                                <tr>
                                    <th></th>
                                    <td colspan="5">User has no certifications</td>
                                </tr>
                            @endif
                      </tbody>
                    </table>

                </div>

                <!-- EYE TESTS NAV -->
                <div class="tab-pane fade" id="nav-eye_tests" role="tabpanel" aria-labelledby="nav-eye_tests-tab">

                    <br>

                    <!-- Add EyeTest Form -->
                    @if (count($eye_tests) == 0)

                        {!! Form::open(['method'=>'GET', 'action'=>['EyeTestController@create'], 'files'=>false]) !!}

                            {!! Form::hidden('user_id', $user->id) !!}

                            {!! Form::submit('Add Eye Test', ['class'=>'btn btn-primary']) !!}

                        {!! Form::close() !!}

                    @endif

                    <br>

                    <table class="table tabel-sm table-hover">

                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Certified By</th>
                                <th scope="col">Document</th>
                                <th scope="col">Status</th>
                                <th scope="col">Test Date</th>
                                <th scope="col">Expiry Date</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if (count($eye_tests) > 0)

                                @foreach ($eye_tests as $eye_test)
                                    <tr>
                                        <th scope="row">{{$eye_test->id}}</th>
                                        <td>{{$eye_test->certified_by}}</td>
                                        <td><a href="{{route('eye_tests.download_document', $eye_test)}}">{{$eye_test->document_path ? 'doc' : ''}}</a></td>
                                        <td>{{$eye_test->read_status}}</td>
                                        <td>{{$eye_test->test_date}}</td>
                                        <td>{{$eye_test->expiry_date}}</td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                                <!-- @can('create', $eye_test)
                                                    {!! Form::open(['method'=>'GET', 'action'=>['EyeTestController@create', $eye_test], 'files'=>false]) !!}

                                                        {!! Form::hidden('user_id', $user->id) !!}

                                                        {!! Form::hidden('eye_test_id', $eye_test->id) !!}

                                                        {!! Form::submit('Renew', ['class'=>'btn btn-success btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan -->
                                                @can('update', $eye_test)
                                                    {!! Form::open(['method'=>'GET', 'action'=>['EyeTestController@edit', $eye_test], 'files'=>false]) !!}

                                                        {!! Form::submit('Edit', ['class'=>'btn btn-secondary btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan
                                                @can('delete', $eye_test)
                                                    {!! Form::open(['method'=>'DELETE', 'action'=>['EyeTestController@destroy', $eye_test], 'files'=>false]) !!}

                                                        {!! Form::submit('Delete', ['class'=>'btn btn-danger btn-sm']) !!}

                                                    {!! Form::close() !!}
                                                @endcan
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                            @else
                                <tr>
                                    <th></th>
                                    <td colspan="5">User has no eye-tests</td>
                                </tr>
                            @endif
                      </tbody>
                    </table>

                </div>

            </div>

        </div>

        <!-- ACTIONS -->
        <div class="col-3 border border-info rounded">

            <br>

            <a href="{{route('users.create')}}" class="btn btn-primary btn-block">Add User</a>

            <br>

            {!! Form::open(['method'=>'DELETE', 'action'=>['UserController@destroy', $user], 'files'=>false]) !!}

            {!! Form::submit('Delete User', ['class'=>'btn btn-danger btn-block']) !!}

            {!! Form::close() !!}

            <br>

        </div>

    </div>





@stop
