@extends('layouts.app')

@section('content')

<h1>Create User</h1>

{!! Form::open(['method'=>'POST', 'action'=>['UserController@store'], 'files'=>false]) !!}

    <div class='form-group'>
        {!! Form::label('employee_no', 'Employee Number:') !!}
        {!! Form::text('employee_no', null, ['class'=>'form-control']) !!}
        @error('employee_no')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('name', 'Name:') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
        @error('name')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('email', 'Email:') !!}
        {!! Form::text('email', null, ['class'=>'form-control']) !!}
        @error('email')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('mobile', 'Mobile:') !!}
        {!! Form::text('mobile', null, ['class'=>'form-control']) !!}
        @error('mobile')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <div class='form-group'>
        {!! Form::label('identity', 'ID Number:') !!}
        {!! Form::text('indentity', null, ['class'=>'form-control']) !!}
        @error('identity')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    {!! Form::label('Address:') !!}
    <div class='form-group'>
        {!! Form::text('street_number', null, ['class'=>'form-control', 'placeholder'=>'Street Number']) !!}
        @error('street_number')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('suburb', null, ['class'=>'form-control', 'placeholder'=>'Suburb']) !!}
        @error('suburb')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('city', null, ['class'=>'form-control', 'placeholder'=>'City']) !!}
        @error('city')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('province', null, ['class'=>'form-control', 'placeholder'=>'Province']) !!}
        @error('province')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>
    <div class='form-group'>
        {!! Form::text('postal_code', null, ['class'=>'form-control', 'placeholder'=>'Postal Code']) !!}
        @error('postal_code')
            <p class='text-danger'>{{$message}}</p>
        @enderror
    </div>

    <br>

    {!! Form::label('Admin User:') !!}
    <div class='form-check'>
        {!! Form::radio('is_admin', '1', ['class'=>'form-check-input']) !!}
        {!! Form::label('is_admin', 'Admin') !!}
    </div>
    <div class='form-check'>
        {!! Form::radio('is_admin', '0', ['class'=>'form-check-input']) !!}
        {!! Form::label('is_admin', 'Normal Personnel') !!}
    </div>

    <br>

    {!! Form::submit('Create', ['class'=>'btn btn-primary']) !!}


{!! Form::close() !!}

@stop
