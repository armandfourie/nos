@extends('layouts.app')

@section('content')

    <div class="row justify-content-between">

        <!-- DISPLAY CONTENT -->
        <div class="col-8">

            <h1>{{$certification_type->method}}</h1>

            <p>Authority: {{$certification_type->authority}}</p>
            <p>Code: {{$certification_type->code}}</p>
            <p>Level: {{$certification_type->level}}</p>

            <p>Expires: {{$certification_type->read_expiry}}</p>

            <p>Description:</p>
            <p>{{$certification_type->description}}</p>



        </div>

        <!-- ACTIONS -->
        @can('update', $certification_type)
        <div class="col-3 border border-info rounded">

            <br>

            <a href="{{route('certification_types.edit', $certification_type)}}" class="btn btn-secondary btn-block">Edit</a>

            <br>

            {!! Form::open(['method'=>'DELETE', 'action'=>['CertificationTypeController@destroy', $certification_type], 'files'=>false]) !!}

            {!! Form::submit('Delete Certification', ['class'=>'btn btn-danger btn-block']) !!}

            {!! Form::close() !!}

            <br>

        </div>
        @endcan


    </div>


@stop
