@extends('layouts.app')

@section('content')

    <h1>Edit Certification</h1>

    <div class="col-8">
        {!! Form::model($certification_type, ['method'=>'PATCH', 'action'=>['CertificationTypeController@update', $certification_type], 'files'=>false]) !!}

        <div class='form-group'>
            {!! Form::label('method', 'Method:') !!}
            {!! Form::text('method', null, ['class'=>'form-control']) !!}
            @error('method')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('code', 'Short Code:') !!}
            {!! Form::text('code', null, ['class'=>'form-control']) !!}
            @error('code')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class'=>'form-control ', 'rows'=>3]) !!}
            @error('description')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('level', 'Level:') !!}
            {!! Form::number('level', null, ['class'=>'form-control']) !!}
            @error('level')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('authority', 'Authority:') !!}
            {!! Form::text('authority', null, ['class'=>'form-control']) !!}
            @error('authority')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

        <div class='form-group'>
            {!! Form::label('months_valid', 'Months Valid:') !!}
            {!! Form::number('months_valid', null, ['class'=>'form-control']) !!}
            @error('months_valid')
                <p class='text-danger'>{{$message}}</p>
            @enderror
        </div>

            {!! Form::submit('Save Changes', ['class'=>'btn btn-primary']) !!}

        {!! Form::close() !!}

    </div>

@stop
