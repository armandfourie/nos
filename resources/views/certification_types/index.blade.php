@extends('layouts.app')

@section('content')

<h1>Certification Types</h1>

<div class="row justify-content-between">

    <!-- CREATE FORM -->
    <div class="col-6">

        {!! Form::open(['method'=>'POST', 'action'=>['CertificationTypeController@store'], 'files'=>false]) !!}

            <div class='form-group'>
                {!! Form::label('method', 'Method:') !!}
                {!! Form::text('method', null, ['class'=>'form-control']) !!}
                @error('method')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            <div class='form-group'>
                {!! Form::label('code', 'Short Code:') !!}
                {!! Form::text('code', null, ['class'=>'form-control']) !!}
                @error('code')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            <div class='form-group'>
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description', null, ['class'=>'form-control ', 'rows'=>3]) !!}
                @error('description')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            <div class='form-group'>
                {!! Form::label('level', 'Level:') !!}
                {!! Form::number('level', null, ['class'=>'form-control']) !!}
                @error('level')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            <div class='form-group'>
                {!! Form::label('authority', 'Authority:') !!}
                {!! Form::text('authority', null, ['class'=>'form-control']) !!}
                @error('authority')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            <div class='form-group'>
                {!! Form::label('months_valid', 'Months Valid:') !!}
                {!! Form::number('months_valid', null, ['class'=>'form-control']) !!}
                @error('months_valid')
                    <p class='text-danger'>{{$message}}</p>
                @enderror
            </div>

            {!! Form::submit('Create New', ['class'=>'btn btn-primary']) !!}

        {!! Form::close() !!}

    </div>

    <!-- INDEX TABLE -->
    <div class="col-5">

        <table class="table tabel-sm table-hover">

            <thead>
                <tr>
                    <th scope="col">Code</th>
                    <th scope="col">Method</th>
                </tr>
            </thead>

            <tbody>
                @if (count($certification_types) > 0)

                    @foreach ($certification_types as $certification_type)
                        <tr>
                            <th scope="row"><a href="{{route('certification_types.show', $certification_type)}}">{{$certification_type->code_level}}</a></th>
                            <td>{{$certification_type->method}}</td>
                        </tr>
                    @endforeach

                @else
                    <tr>
                        <th></th>
                        <td colspan="5">There are no certifications registered</td>
                    </tr>
                @endif
          </tbody>
        </table>


    </div>

</div>

@stop
