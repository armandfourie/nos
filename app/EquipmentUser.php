<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EquipmentUser extends Model
{
    // Properties
    protected $fillable = [
        'user_id', 'equipment_id', 'issue_date', 'planned_return_date', 'return_date', 'comments'
    ];
}
