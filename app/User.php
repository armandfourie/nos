<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Filters\UserFilter;
use Illuminate\Database\Eloquent\Builder;
use App\Certification;
use App\UserAddress;
use App\EyeTest;
use App\Equipment;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_no', 'name', 'email', 'mobile', 'password', 'is_admin', 'identity',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_admin' => 'boolean',
    ];

    public function scopeFilter(Builder $builder, $request)
    {
        return (new UserFilter($request))->filter($builder);
    }

    // Relationships

    public function certifications()
    {
        return $this->hasMany(Certification::class);
    }

    public function address()
    {
        return $this->hasOne(UserAddress::class);
    }

    public function eyeTests()
    {
        return $this->hasMany(EyeTest::class);
    }

    public function equipment()
    {
        return $this->belongsToMany(Equipment::class, 'equipment_users')->withPivot('id', 'issue_date', 'planned_return_date', 'return_date');
    }
}
