<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CertificationType;
use App\User;
use Carbon\Carbon;

class Certification extends Model
{

    // Status database constants
    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRED = 2;

    // Properties
    protected $readStatuses = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_EXPIRED => 'Expired',
    ];

    protected $fillable = [
        'certification_type_id', 'user_id', 'certified_by', 'document_path', 'certification_date', 'expiry_date', 'status', 'renewal_for_id', 'recertification_for_id',
    ];

    protected $reminder_period = 3; //months

    // Relationships
    public function certificationType()
    {
        return $this->belongsTo(CertificationType::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function renewal()
    {
        return $this->hasOne(self::class, 'renewal_for_id');
    }

    public function recertification()
    {
        return $this->hasOne(self::class, 'recertification_for_id');
    }

    // Accessors
    public function getTypeAttribute()
    {
        return $this->certificationType->method;
    }

    public function getReadStatusAttribute()
    {
        return $this->readStatuses[$this->status];
    }

    public function getCodeAttribute()
    {
        return $this->certificationType->code_level;
    }

    public function getDocumentStorePathAttribute()
    {
        return '/users/' . $this->user_id . '/certifications';
    }

    // Mutators



    // Scopes
    public function scopeGetRenewalRequired($query)
    {
        $date = Carbon::parse(date('Y-m-d'))->addMonths($this->reminder_period);
        return $query->where('renewal_for_id', null)
                        ->where('expiry_date', '<', $date)
                        ->whereDoesntHave('renewal')
                        ->get();
    }

    public function scopeGetRecertificationRequired($query)
    {
        $date = Carbon::parse(date('Y-m-d'))->addMonths($this->reminder_period);
        return $query->where('renewal_for_id', '!=', null)
                        ->where('expiry_date', '<', $date)
                        ->whereDoesntHave('recertification')
                        ->get();
    }

    public function scopeGetExpires($query)
    {
        $date = Carbon::parse(date('Y-m-d'));
        return $query->where('expiry_date', '<', $date)
                        ->get();
    }

    // Context
    public function requiresRenewal()
    {
        if ($this->renewal_for_id == null)
        {
            return true;
        }
    }

    public function requiresRecertification()
    {
        if ($this->renewal_for_id != null)
        {
            return true;
        }
    }
}
