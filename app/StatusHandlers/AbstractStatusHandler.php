<?php

//AbstractStatusHandler.php

namespace App\StatusHandlers;

abstract class AbstractStatusHandler
{
    // properties
    protected $currentStates = [
        //
    ];

    // SET STATUS
    public function setStatus($model, $status)
    {
        if ($this->canSetStatus($model, $status)) {
            $model->status = $status;
            $model->update();
        }
    }

    // PROTECTED METHODS
    protected function canSetStatus($model, $status)
    {
        $currentState = $this->getCurrentStateClass($model);

        if (!$newState = $this->getNewAvailableStateClass($currentState, $status)) {
            return false;
        } else {
            return $newState->canSet($model);
        }
    }

    protected function getCurrentStateClass($model)
    {
        return new $this->currentStates[$model->status];
    }

    // see whether new status is in availableStates
    // if YES -> return AvailableState Class object
    // if NO -> return null
    protected function getNewAvailableStateClass($currentState, $status)
    {
        if (array_key_exists($status, $currentState->availableStates)) {
            return new $currentState->availableStates[$status];
        } else {
            return false;
        }
    }
}
