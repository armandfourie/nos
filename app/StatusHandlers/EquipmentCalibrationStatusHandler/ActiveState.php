<?php

// ActiveState.php

namespace App\StatusHandlers\EquipmentCalibrationStatusHandler;

use App\EquipmentCalibration;
use App\StatusHandlers\EquipmentCalibrationStatusHandler\AvailableStates\Expired;

class ActiveState
{
    // properties
    public $availableStates = [
        EquipmentCalibration::STATUS_EXPIRED => Expired::class,
    ];

}
