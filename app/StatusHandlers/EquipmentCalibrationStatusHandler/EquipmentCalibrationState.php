<?php

//EyeTestState.php

namespace App\StatusHandlers\EquipmentCalibrationStatusHandler;

use App\StatusHandlers\AbstractStatusHandler;
use App\EquipmentCalibration;
use App\StatusHandlers\EquipmentCalibrationStatusHandler\ActiveState;
use App\StatusHandlers\EquipmentCalibrationStatusHandler\ExpiredState;

class EquipmentCalibrationState extends AbstractStatusHandler
{
    // properties
    protected $currentStates = [
        EquipmentCalibration::STATUS_ACTIVE => ActiveState::class,
        EquipmentCalibration::STATUS_EXPIRED => ExpiredState::class,
    ];
}
