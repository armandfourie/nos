<?php

// Expired.php

namespace App\StatusHandlers\EquipmentCalibrationStatusHandler\AvailableStates;

use App\EquipmentCalibration;
use Carbon\Carbon;

class Expired
{
    public function canSet(EquipmentCalibration $equipment_calibration)
    {
        if (date('Y-m-d') > Carbon::parse($equipment_calibration->expiry_date) || $equipment_calibration->renewal) 
        {
            return true;
        }
        return false;
    }
}
