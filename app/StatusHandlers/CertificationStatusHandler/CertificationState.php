<?php

//CertificationState.php

namespace App\StatusHandlers\CertificationStatusHandler;

use App\StatusHandlers\AbstractStatusHandler;
use App\Certification;
use App\StatusHandlers\CertificationStatusHandler\ActiveState;
use App\StatusHandlers\CertificationStatusHandler\ExpiredState;

class CertificationState extends AbstractStatusHandler
{
    // properties
    protected $currentStates = [
        Certification::STATUS_ACTIVE => ActiveState::class,
        Certification::STATUS_EXPIRED => ExpiredState::class,
    ];
}
