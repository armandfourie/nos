<?php

// ActiveState.php

namespace App\StatusHandlers\CertificationStatusHandler;

use App\Certification;
use App\StatusHandlers\CertificationStatusHandler\AvailableStates\Expired;

class ActiveState
{
    // properties
    public $availableStates = [
        Certification::STATUS_EXPIRED => Expired::class,
    ];

}
