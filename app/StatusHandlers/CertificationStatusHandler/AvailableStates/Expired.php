<?php

// Expired.php

namespace App\StatusHandlers\CertificationStatusHandler\AvailableStates;

use App\Certification;
use Carbon\Carbon;

class Expired
{
    public function canSet(Certification $certification)
    {
        if (date('Y-m-d') > Carbon::parse($certification->expiry_date) || $certification->renewal || $certification->recertification) {
            return true;
        }
        return false;
    }
}
