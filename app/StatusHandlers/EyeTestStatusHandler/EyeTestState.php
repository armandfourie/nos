<?php

//EyeTestState.php

namespace App\StatusHandlers\EyeTestStatusHandler;

use App\StatusHandlers\AbstractStatusHandler;
use App\EyeTest;
use App\StatusHandlers\EyeTestStatusHandler\ActiveState;
use App\StatusHandlers\EyeTestStatusHandler\ExpiredState;

class EyeTestState extends AbstractStatusHandler
{
    // properties
    protected $currentStates = [
        EyeTest::STATUS_ACTIVE => ActiveState::class,
        EyeTest::STATUS_EXPIRED => ExpiredState::class,
    ];
}
