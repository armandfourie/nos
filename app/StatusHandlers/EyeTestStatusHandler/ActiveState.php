<?php

// ActiveState.php

namespace App\StatusHandlers\EyeTestStatusHandler;

use App\EyeTest;
use App\StatusHandlers\EyeTestStatusHandler\AvailableStates\Expired;

class ActiveState
{
    // properties
    public $availableStates = [
        EyeTest::STATUS_EXPIRED => Expired::class,
    ];

}
