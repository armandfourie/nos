<?php

// Expired.php

namespace App\StatusHandlers\EyeTestStatusHandler\AvailableStates;

use App\EyeTest;
use Carbon\Carbon;

class Expired
{
    public function canSet(EyeTest $eye_test)
    {
        if (date('Y-m-d') > Carbon::parse($eye_test->expiry_date) || $eye_test->renewal) {
            return true;
        }
        return false;
    }
}
