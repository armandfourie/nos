<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Certification;
use App\EyeTest;
use App\EquipmentCalibration;
use App\StatusHandlers\CertificationStatusHandler\CertificationState;
use App\StatusHandlers\EyeTestStatusHandler\EyeTestState;
use App\StatusHandlers\EquipmentCalibrationStatusHandler\EquipmentCalibrationState;

class SetExpiredStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'set-status:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(CertificationState $certificationLogic, EyeTestState $eyeTestLogic, EquipmentCalibrationState $equipmentCalibrationLogic)
    {
        $this->certificationLogic = $certificationLogic;
        $this->eyeTestLogic = $eyeTestLogic;
        $this->equipmentCalibrationLogic = $equipmentCalibrationLogic;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // expiring certifications
        $certifications = Certification::getExpires();

        foreach ($certifications as $certification)
        {
            $this->certificationLogic->setStatus($certification, Certification::STATUS_EXPIRED);
        }

        // expiring eye_tests
        $eye_tests = EyeTest::getExpires();

        foreach ($eye_tests as $eye_test)
        {
            $this->eyeTestLogic->setStatus($eye_test, EyeTest::STATUS_EXPIRED);
        }

        // expiring equipment_calibrations
        $equipment_calibrations = EquipmentCalibration::getExpires();

        foreach ($equipment_calibrations as $equipment_calibration)
        {
            $this->equipmentCalibrationLogic->setStatus($equipment_calibration, EquipmentCalibration::STATUS_EXPIRED);
        }

    }
}
