<?php

// UserFilter.php

namespace App\Filters;

use App\Filters\IdFilter;

class UserFilter extends AbstractFilter
{
    protected $filters = [
        'id' => IdFilter::class,
    ];
}
