<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class EyeTest extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRED = 2;

    const MONTHS_VALID = 12;

    // Properties
    protected $fillable = [
        'user_id', 'certified_by', 'status', 'test_date', 'expiry_date', 'renewal_for_id'
    ];

    protected $readStatuses = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_EXPIRED => 'Expired',
    ];

    protected $reminder_period = 3;

    // Relationships
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function renewal()
    {
        return $this->hasOne(self::class, 'renewal_for_id');
    }

    // Accessors
    public function getReadStatusAttribute()
    {
        return $this->readStatuses[$this->status];
    }

    public function getDocumentStorePathAttribute()
    {
        return '/users/' . $this->user_id . '/eye_tests';
    }

    // Scope Filters
    public function scopeGetRequiresRenewal($query)
    {
        $date = Carbon::parse(date('Y-m-d'))->addMonths($this->reminder_period);

        return $query->where('expiry_date', '<', $date)
                        ->whereDoesntHave('renewal')
                        ->get();
    }

    public function scopeGetExpires($query)
    {
        $date = Carbon::parse(date('Y-m-d'));

        return $query->where('expiry_date', '<', $date)
                        ->get();
    }

    // Context
    public function requiresRenewal()
    {
        if ($this->renewal_for_id == null) {
            return true;
        }
    }

}
