<?php

namespace App\Policies;

use App\EyeTest;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EyeTestPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any eye tests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the eye test.
     *
     * @param  \App\User  $user
     * @param  \App\EyeTest  $eyeTest
     * @return mixed
     */
    public function view(User $user, EyeTest $eyeTest)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can create eye tests.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the eye test.
     *
     * @param  \App\User  $user
     * @param  \App\EyeTest  $eyeTest
     * @return mixed
     */
    public function update(User $user, EyeTest $eyeTest)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the eye test.
     *
     * @param  \App\User  $user
     * @param  \App\EyeTest  $eyeTest
     * @return mixed
     */
    public function delete(User $user, EyeTest $eyeTest)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the eye test.
     *
     * @param  \App\User  $user
     * @param  \App\EyeTest  $eyeTest
     * @return mixed
     */
    public function restore(User $user, EyeTest $eyeTest)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the eye test.
     *
     * @param  \App\User  $user
     * @param  \App\EyeTest  $eyeTest
     * @return mixed
     */
    public function forceDelete(User $user, EyeTest $eyeTest)
    {
        //
    }
}
