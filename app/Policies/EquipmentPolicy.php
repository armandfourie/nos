<?php

namespace App\Policies;

use App\Equipment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EquipmentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any equipment.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can view the equipment.
     *
     * @param  \App\User  $user
     * @param  \App\Equipment  $equipment
     * @return mixed
     */
    public function view(User $user, Equipment $equipment)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can create equipment.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the equipment.
     *
     * @param  \App\User  $user
     * @param  \App\Equipment  $equipment
     * @return mixed
     */
    public function update(User $user, Equipment $equipment)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the equipment.
     *
     * @param  \App\User  $user
     * @param  \App\Equipment  $equipment
     * @return mixed
     */
    public function delete(User $user, Equipment $equipment)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the equipment.
     *
     * @param  \App\User  $user
     * @param  \App\Equipment  $equipment
     * @return mixed
     */
    public function restore(User $user, Equipment $equipment)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the equipment.
     *
     * @param  \App\User  $user
     * @param  \App\Equipment  $equipment
     * @return mixed
     */
    public function forceDelete(User $user, Equipment $equipment)
    {
        //
    }
}
