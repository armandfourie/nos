<?php

namespace App\Policies;

use App\EquipmentUser;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EquipmentUserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any equipment users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can view the equipment user.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentUser  $equipmentUser
     * @return mixed
     */
    public function view(User $user, EquipmentUser $equipmentUser)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can create equipment users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the equipment user.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentUser  $equipmentUser
     * @return mixed
     */
    public function update(User $user, EquipmentUser $equipmentUser)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the equipment user.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentUser  $equipmentUser
     * @return mixed
     */
    public function delete(User $user, EquipmentUser $equipmentUser)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the equipment user.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentUser  $equipmentUser
     * @return mixed
     */
    public function restore(User $user, EquipmentUser $equipmentUser)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the equipment user.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentUser  $equipmentUser
     * @return mixed
     */
    public function forceDelete(User $user, EquipmentUser $equipmentUser)
    {
        //
    }
}
