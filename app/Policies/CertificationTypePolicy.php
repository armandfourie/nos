<?php

namespace App\Policies;

use App\CertificationType;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificationTypePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any certification types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can view the certification type.
     *
     * @param  \App\User  $user
     * @param  \App\CertificationType  $certificationType
     * @return mixed
     */
    public function view(User $user, CertificationType $certificationType)
    {
        return true;
    }

    /**
     * Determine whether the user can create certification types.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the certification type.
     *
     * @param  \App\User  $user
     * @param  \App\CertificationType  $certificationType
     * @return mixed
     */
    public function update(User $user, CertificationType $certificationType)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the certification type.
     *
     * @param  \App\User  $user
     * @param  \App\CertificationType  $certificationType
     * @return mixed
     */
    public function delete(User $user, CertificationType $certificationType)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the certification type.
     *
     * @param  \App\User  $user
     * @param  \App\CertificationType  $certificationType
     * @return mixed
     */
    public function restore(User $user, CertificationType $certificationType)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the certification type.
     *
     * @param  \App\User  $user
     * @param  \App\CertificationType  $certificationType
     * @return mixed
     */
    public function forceDelete(User $user, CertificationType $certificationType)
    {
        //
    }
}
