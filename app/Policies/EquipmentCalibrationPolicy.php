<?php

namespace App\Policies;

use App\EquipmentCalibration;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EquipmentCalibrationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any equipment calibrations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can view the equipment calibration.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentCalibration  $equipmentCalibration
     * @return mixed
     */
    public function view(User $user, EquipmentCalibration $equipmentCalibration)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can create equipment calibrations.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the equipment calibration.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentCalibration  $equipmentCalibration
     * @return mixed
     */
    public function update(User $user, EquipmentCalibration $equipmentCalibration)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can delete the equipment calibration.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentCalibration  $equipmentCalibration
     * @return mixed
     */
    public function delete(User $user, EquipmentCalibration $equipmentCalibration)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can restore the equipment calibration.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentCalibration  $equipmentCalibration
     * @return mixed
     */
    public function restore(User $user, EquipmentCalibration $equipmentCalibration)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the equipment calibration.
     *
     * @param  \App\User  $user
     * @param  \App\EquipmentCalibration  $equipmentCalibration
     * @return mixed
     */
    public function forceDelete(User $user, EquipmentCalibration $equipmentCalibration)
    {
        //
    }
}
