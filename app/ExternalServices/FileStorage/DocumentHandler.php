<?php

// DocumentHandler.php

namespace App\ExternalServices\FileStorage;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentHandler
{
    public static function store($document_path, $file)
    {
        return Storage::disk('s3')->put($document_path, $file);
    }

    public static function update($old_document_path, $document_path, $file)
    {
        Storage::disk('s3')->delete($old_document_path);
        return Storage::disk('s3')->put($document_path, $file);
    }

    public static function delete($document_path)
    {
        return Storage::disk('s3')->delete($document_path);
    }

    public static function download($document_path)
    {
        return Storage::disk('s3')->download($document_path);
    }
}
