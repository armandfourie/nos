<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = [
        'user_id', 'street_number', 'suburb', 'city', 'province', 'postal_code',
    ];
}
