<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\EquipmentCalibration;
use App\User;
use Carbon\Carbon;

class Equipment extends Model
{
    // Properties
    protected $fillable = [
        'asset_number', 'type', 'manufacturer', 'model', 'serial_number', 'calibration_months_valid'
    ];

    // Relationships
    public function calibrations()
    {
        return $this->hasMany(EquipmentCalibration::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'equipment_users')->withPivot('id', 'issue_date', 'planned_return_date', 'return_date');
    }

    // Context
    public function isCalibrated()
    {
        if ($this->calibrations()->where('status', EquipmentCalibration::STATUS_ACTIVE)->count() != 0)
        {
            return true;
        }
        return false;
    }

    public function isNotCheckedOut()
    {
        if ($this->users()->whereNull('return_date')->count() == 0)
        {
            return true;
        }
        return false;
    }
}
