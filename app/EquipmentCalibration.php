<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Equipment;
use Carbon\Carbon;

class EquipmentCalibration extends Model
{
    // Constants
    const STATUS_ACTIVE = 1;
    const STATUS_EXPIRED = 2;

    // Properties
    protected $fillable = [
        'equipment_id', 'calibrated_by', 'document_path', 'calibration_date', 'expiry_date', 'status'
    ];

    protected $readStatuses = [
        self::STATUS_ACTIVE => 'Active',
        self::STATUS_EXPIRED => 'Expired',
    ];

    protected $reminder_period = 3; // Months


    // Relationships
    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    public function renewal()
    {
        return $this->hasOne(self::class, 'renewal_for_id');
    }

    public function renewalFor()
    {
        return $this->belongsTo(self::class, 'renewal_for_id');
    }

    // Accessors
    public function getReadStatusAttribute()
    {
        return $this->readStatuses[$this->status];
    }

    public function getDocumentStorePathAttribute()
    {
        return '/equipment/' . $this->equipment_id . '/calibration_certificates';
    }

    // Scopes
    public function scopeGetExpires($query)
    {
        $date = Carbon::parse(date('Y-m-d'));
        return $query->where('status', self::STATUS_ACTIVE)
                        ->where('expiry_date', '<' , $date)
                        ->orWhereHas('renewal')
                        ->get();
    }

    public function scopeGetRequiresRenewal($query)
    {
        $date = Carbon::parse(date('Y-m-d'))->addMonths($this->reminder_period);
        return $query->where('expiry_date', '<', $date)
                        ->whereDoesntHave('renewal')
                        ->get();
    }

    // Context
    public function requiresRenewal()
    {
        $date = Carbon::parse(date('Y-m-d'))->addMonths($this->reminder_period);
        if ($this->renewal == null && $this->expiry_date < $date)
        {
            return true;
        }
        return false;
    }
}
