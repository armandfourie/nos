<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($user = $this->route('user')) {
            $user_id = $user->id;
        } else {
            $user_id = '';
        }

        return [
            'employee_no' => 'required|unique:users,employee_no,'.$user_id,
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$user_id,
            'mobile' => 'nullable|unique:users,mobile,'.$user_id,
            'identity' => 'nullable|unique:users,identity,'.$user_id,
        ];
    }
}
