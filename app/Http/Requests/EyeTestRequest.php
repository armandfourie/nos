<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\EyeTest;
use Carbon\Carbon;

class EyeTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'sometimes',
            'certified_by' => 'required',
            'status' => 'required',
            'test_date' => 'required|date',
            'expiry_date' => 'required|date',
        ];
    }

    public function prepareForValidation()
    {
        $expiry_date = Carbon::parse($this->test_date)->addMonths(EyeTest::MONTHS_VALID)->addDays(-1)->format('Y-m-d');

        if (Carbon::parse(date('Y-m-d')) < $expiry_date)
        {
            $this->merge([
                'status' => EyeTest::STATUS_ACTIVE,
                'expiry_date' => $expiry_date,
            ]);
        }
        else
        {
            $this->merge([
                'status' => EyeTest::STATUS_EXPIRED,
                'expiry_date' => $expiry_date,
            ]);
        }

    }
}
