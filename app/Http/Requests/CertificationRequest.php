<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\CertificationType;
use Carbon\Carbon;
use App\Certification;

class CertificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'sometimes',
            'certification_type_id' => 'required',
            'certified_by' => 'required',
            'certification_date' => 'required|date',
            'expiry_date' => 'required|date',
        ];
    }

    public function prepareForValidation()
    {

        $certification_type = CertificationType::find($this->certification_type_id);
        $expiry_date = Carbon::parse($this->certification_date)
                        ->addMonths($certification_type->months_valid)->addDay(-1)->format('Y-m-d');

        if (Carbon::parse(date('Y-m-d')) < $expiry_date)
        {
            $this->merge([
                'status' => Certification::STATUS_ACTIVE,
                'expiry_date' => $expiry_date,
            ]);
        }
        else
        {
            $this->merge([
                'status' => Certification::STATUS_EXPIRED,
                'expiry_date' => $expiry_date,
            ]);
        }

    }

}
