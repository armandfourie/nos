<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use App\EquipmentCalibration;

class EquipmentCalibrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'equipment_id' => 'sometimes',
            'calibrated_by' => 'required',
            'calibration_date' => 'required|date',
            'expiry_date' => 'required|date',
            'status' => 'required',
        ];
    }

    public function prepareForValidation()
    {
        if (Carbon::parse(date('Y-m-d')) < $this->expiry_date)
        {
            $this->merge([
                'status' => EquipmentCalibration::STATUS_ACTIVE,
            ]);
        }
        else
        {
            $this->merge([
                'status' => EquipmentCalibration::STATUS_EXPIRED,
            ]);
        }
    }
}
