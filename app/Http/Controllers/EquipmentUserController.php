<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EquipmentUserRequest;
use App\Http\Requests\EquipmentReturnRequest;
use App\EquipmentUser;
use App\Equipment;
use App\User;

class EquipmentUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->authorizeResource(EquipmentUser::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $equipment = Equipment::findOrFail($request->equipment_id);
        $users = User::pluck('name', 'id')->toArray();
        return view('equipment_users.create', compact('equipment', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EquipmentUserRequest $request)
    {
        EquipmentUser::create($request->all());

        return redirect(route('equipment.show', $request->equipment_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EquipmentUser $equipment_user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EquipmentUser $equipment_user)
    {
        $users = User::pluck('name', 'id')->toArray();
        $equipment = Equipment::findOrFail($equipment_user->equipment_id);
        return view('equipment_users.edit', compact('equipment_user', 'users', 'equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EquipmentUser $equipment_user)
    {
        $equipment_user->update($request->all());

        return redirect(route('equipment.show', $equipment_user->equipment_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EquipmentUser $equipment_user)
    {
        $equipment_user->delete();

        return redirect(route('equipment.show', $equipment_user->equipment_id));
    }

    /**
     * Record the return of a piece of equipment.
     *
     * @param EquipmentUser $equipment_user -> specific booking being returned
     * @param Request $request -> containing return date
     * @return \Illuminate\Http\Response
     */
    public function return(EquipmentUser $equipment_user, EquipmentReturnRequest $request)
    {
        $equipment_user->update($request->all());

        return redirect(route('equipment.show', $equipment_user->equipment_id));
    }
}
