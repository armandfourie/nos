<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EyeTest;
use App\User;
use App\Http\Requests\EyeTestRequest;
use App\ModelLogic\EyeTest\EyeTestLogic;
use App\ExternalServices\FileStorage\DocumentHandler;

class EyeTestController extends Controller
{

    public function __construct(EyeTestLogic $eyeTestLogic)
    {
        $this->eyeTestLogic = $eyeTestLogic;
        $this->authorizeResource(EyeTest::class);
        $this->middleware('admin');
    }

    /**
     * Display a listing all user eye tests that are expiring.
     *
     * @return \Illuminate\Http\Response
     */
    public function expiringIndex()
    {
        $expiring_eye_tests = EyeTest::getRequiresRenewal();
        return view('eye_tests.expiring_index', compact('expiring_eye_tests'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = User::find($request->user_id);
        $eye_test = EyeTest::find($request->eye_test_id);

        return view('eye_tests.create', compact('user', 'eye_test'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EyeTestRequest $request)
    {
        $this->eyeTestLogic->create($request);

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EyeTest $eye_test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EyeTest $eye_test)
    {
        return view('eye_tests.edit', compact('eye_test'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EyeTestRequest $request, EyeTest $eye_test)
    {
        $this->eyeTestLogic->update($eye_test, $request);

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EyeTest $eye_test)
    {
        $this->eyeTestLogic->delete($eye_test);

        return redirect(route('users.index'));
    }

    public function downloadDocument(EyeTest $eye_test)
    {
        return DocumentHandler::download($eye_test->document_path);
    }
}
