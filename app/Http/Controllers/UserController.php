<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\UserAddress;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin')->except(['show', 'edit', 'update']);
        $this->authorizeResource(User::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter_users = User::pluck('name', 'id')->toArray();
        $user = User::filter($request)->first();
        $certifications = $user->certifications;
        $eye_tests = $user->eyeTests;
        $address = $user->address;

        return view('users.index', compact('filter_users', 'user', 'certifications', 'eye_tests', 'address'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $user_input = $request->except('street_number', 'suburb', 'city', 'province', 'postal_code');
        $address_input = $request->only('street_number', 'suburb', 'city', 'province', 'postal_code');
        $user_input['password'] = bcrypt('password');

        $user = User::create($user_input);
        $user_address = new UserAddress($address_input);
        $user->address()->save($user_address);

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $address = $user->address;
        return view('users.edit', compact('user', 'address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $user_input = $request->except('street_number', 'suburb', 'city', 'province', 'postal_code');
        $address_input = $request->only('street_number', 'suburb', 'city', 'province', 'postal_code');
        $user->update($user_input);

        if ($user->address){
            $user->address()->update($address_input);
        } else {
            $user_address = new UserAddress($address_input);
            $user->address()->save($user_address);
        }

        return redirect(route('home'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect(route('users.index'));
    }
}
