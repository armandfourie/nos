<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CertificationType;
use App\Http\Requests\CertificationTypeRequest;

class CertificationTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin')->except(['show']);
        $this->authorizeResource(CertificationType::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certification_types = CertificationType::all();

        return view('certification_types.index', compact('certification_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CertificationTypeRequest $request)
    {
        CertificationType::create($request->all());

        return redirect(route('certification_types.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CertificationType $certification_type)
    {
        return view('certification_types.show', compact('certification_type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CertificationType $certification_type)
    {
        return view('certification_types.edit', compact('certification_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CertificationTypeRequest $request, CertificationType $certification_type)
    {
        $certification_type->update($request->all());

        return redirect(route('certification_types.show', $certification_type));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CertificationType $certification_type)
    {
        $certification_type->delete();

        return redirect(route('certification_types.index'));
    }
}
