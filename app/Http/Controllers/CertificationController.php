<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Certification;
use App\CertificationType;
use App\User;
use App\Http\Requests\CertificationRequest;
use App\StatusHandlers\CertificationStatusHandler\CertificationState;
use App\ModelLogic\Certification\CertificationLogic;
use App\ExternalServices\FileStorage\DocumentHandler;


class CertificationController extends Controller
{
    public function __construct(CertificationState $statusLogic, CertificationLogic $certificationLogic)
    {
        $this->statusHandler = $statusLogic;
        $this->certificationLogic = $certificationLogic;
        $this->middleware('admin');
        $this->authorizeResource(Certification::class);
    }

    /**
    * Return an index of all certifications that require renewal or re-certification
    *
    * @return Illuminate\Http\Response
    */
    public function expiringIndex()
    {
        $certifications_renewal = Certification::getRenewalRequired();
        $certifications_recertification = Certification::getRecertificationRequired();

        return view('certifications.expiring_index', compact('certifications_renewal', 'certifications_recertification'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Requests $request
     *
     * @return \Illuminate\Http\Response
     * passes to form
     * user_id -> user to receive certification
     * certification_id -> used in context of recertification and renewals
     * certification_types -> optional CertificationType
     */
    public function create(Request $request)
    {
        $user = User::findOrFail($request->user_id);
        $certification = Certification::find($request->certification_id); // for recertifications or renewals
        $certification_types = CertificationType::all()->pluck('code_level', 'id')->toArray();

        return view('certifications.create', compact('user', 'certification', 'certification_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CertificationRequest $request)
    {
        $this->certificationLogic->create($request);

        return redirect(route('users.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Certification $certification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Certification $certification)
    {
        return view('certifications.edit', compact('certification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CertificationRequest $request, Certification $certification)
    {
        $this->certificationLogic->update($request, $certification);

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certification $certification)
    {
        $this->certificationLogic->delete($certification);

        return redirect(route('users.index'));
    }

    public function downloadDocument(Certification $certification)
    {
        return DocumentHandler::download($certification->document_path);
    }

}
