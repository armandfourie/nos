<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EquipmentCalibrationRequest;
use App\EquipmentCalibration;
use App\Equipment;
use App\ModelLogic\EquipmentCalibration\EquipmentCalibrationLogic;
use App\ExternalServices\FileStorage\DocumentHandler;

class EquipmentCalibrationController extends Controller
{
    public function __construct(EquipmentCalibrationLogic $equipmentCalibrationLogic)
    {
        $this->equipmentCalibrationLogic = $equipmentCalibrationLogic;
        $this->middleware('admin');
        $this->authorizeResource(EquipmentCalibration::class);
    }

    public function expiringIndex()
    {
        $equipment_calibrations = EquipmentCalibration::getRequiresRenewal();

        return view('equipment_calibrations.expiring_index', compact('equipment_calibrations'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $equipment = Equipment::findOrFail($request->equipment_id);
        $equipment_calibration = EquipmentCalibration::find($request->equipment_calibration_id); // in the case of renewals

        return view('equipment_calibrations.create', compact('equipment', 'equipment_calibration'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EquipmentCalibrationRequest $request)
    {
        $this->equipmentCalibrationLogic->create($request);

        return redirect(route('equipment.show', $request->equipment_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(EquipmentCalibration $equipment_calibration)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(EquipmentCalibration $equipment_calibration)
    {
        return view('equipment_calibrations.edit', compact('equipment_calibration'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EquipmentCalibrationRequest $request, EquipmentCalibration $equipment_calibration)
    {
        $this->equipmentCalibrationLogic->update($equipment_calibration, $request);

        return redirect(route('equipment.show', $equipment_calibration->equipment_id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EquipmentCalibration $equipment_calibration)
    {
        $this->equipmentCalibrationLogic->delete($equipment_calibration);

        return redirect(route('equipment.show', $equipment_calibration->equipment_id));
    }

    // Download document
    public function downloadDocument(EquipmentCalibration $equipment_calibration)
    {
        return DocumentHandler::download($equipment_calibration->document_path);
    }
}
