<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificationType extends Model
{

    protected $fillable = [
        'method', 'code', 'description', 'level', 'authority', 'months_valid',
    ];

    protected $appends = [
        'code_level'
    ];

    // Accessors
    public function getReadExpiryAttribute()
    {
        return "This certification needs to be renewed every " . $this->months_valid . " months";
    }

    public function getCodeLevelAttribute()
    {
        return $this->code.'-'.$this->level;
    }
}
