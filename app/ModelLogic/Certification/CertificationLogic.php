<?php

// CertificationLogic.php

namespace App\ModelLogic\Certification;

use App\Certification;
use Illuminate\Http\Request;
use App\ExternalServices\FileStorage\DocumentHandler;
use App\StatusHandlers\CertificationStatusHandler\CertificationState;

class CertificationLogic
{
    public function __construct(CertificationState $certificationStatusLogic)
    {
        $this->certificationStatusLogic = $certificationStatusLogic;
    }

    public function create(Request $request)
    {
        $certification = Certification::find($request->certification_id); // to be renewed or re-certified
        $new_certification = new Certification($request->except('certification_id', 'file'));

        // Document Storage
        if ($file = $request->file('file'))
        {
            $document_path = $new_certification->document_store_path;
            $new_certification->document_path = DocumentHandler::store($document_path, $file);
        }

        // Store Certification
        if ($certification == null)
        {
            $new_certification->save();
        }

        else if ($certification->requiresRecertification())
        {
            $new_certification->recertification_for_id = $request->certification_id;
            $new_certification->save();
            $this->certificationStatusLogic->setStatus($certification, Certification::STATUS_EXPIRED);
        }

        else if ($certification->requiresRenewal())
        {
            $new_certification->renewal_for_id = $request->certification_id;
            $new_certification->save();
            $this->certificationStatusLogic->setStatus($certification, Certification::STATUS_EXPIRED);
        }
    }

    public function update(Request $request, Certification $certification)
    {
        // Document Update
        if ($file = $request->file('file'))
        {
            $document_path = $certification->document_store_path;
            $certification->document_path = DocumentHandler::update($certification->document_path, $document_path, $file);
        }

        $certification->update($request->all());
    }

    public function delete(Certification $certification)
    {
        DocumentHandler::delete($certification->document_path);
        $certification->delete();
    }
}
