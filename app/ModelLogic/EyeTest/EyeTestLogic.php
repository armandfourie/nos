<?php

// EyeTestLogic.php

namespace App\ModelLogic\EyeTest;

use App\EyeTest;
use Illuminate\Http\Request;
use App\ExternalServices\FileStorage\DocumentHandler;
use App\StatusHandlers\EyeTestStatusHandler\EyeTestState;

class EyeTestLogic
{
    public function __construct(EyeTestState $eyeTestStatusLogic)
    {
        $this->eyeTestStatusLogic = $eyeTestStatusLogic;
    }

    public function create(Request $request)
    {
        $eye_test = EyeTest::find($request->eye_test_id); // to be renewed when applicable
        $new_eye_test = new EyeTest($request->except('eye_test_id', 'file'));

        if ($file = $request->file('file'))
        {
            $document_path = $new_eye_test->document_store_path;
            $new_eye_test->document_path = DocumentHandler::store($document_path, $file);
        }

        if ($eye_test == null)
        {
            $new_eye_test->save();
        }
        else
        {
            $new_eye_test->renewal_for_id = $eye_test->id;
            $new_eye_test->save();
            $this->eyeTestStatusLogic->setStatus($eye_test, EyeTest::STATUS_EXPIRED);
        }
    }

    public function update(EyeTest $eye_test, Request $request)
    {
        if ($file = $request->file('file'))
        {
            $eye_test->document_path = DocumentHandler::update($eye_test->document_path, $eye_test->document_store_path, $file);
        }

        $eye_test->update($request->all());
    }

    public function delete($eye_test)
    {
        DocumentHandler::delete($eye_test->document_path);
        $eye_test->delete();
    }
}
