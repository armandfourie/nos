<?php

// EquipmentCalibrationLogic.php

namespace App\ModelLogic\EquipmentCalibration;

use App\EquipmentCalibration;
use Illuminate\Http\Request;
use App\ExternalServices\FileStorage\DocumentHandler;
use App\StatusHandlers\EquipmentCalibrationStatusHandler\EquipmentCalibrationState;

class EquipmentCalibrationLogic
{
    public function __construct(EquipmentCalibrationState $equipmentCalibrationStatusLogic)
    {
        $this->equipmentCalibrationStatusLogic = $equipmentCalibrationStatusLogic;
    }

    public function create(Request $request)
    {
        $equipment_calibration = EquipmentCalibration::find($request->equipment_calibration_id); // to be renewed when applicable
        $new_equipment_calibration = new EquipmentCalibration($request->except('calibration_id', 'file'));

        if ($file = $request->file('file'))
        {
            $document_path = $new_equipment_calibration->document_store_path;
            $new_equipment_calibration->document_path = DocumentHandler::store($document_path, $file);
        }

        if ($equipment_calibration == null)
        {
            $new_equipment_calibration->save();
        }
        else
        {
            $new_equipment_calibration->renewal_for_id = $equipment_calibration->id;
            $new_equipment_calibration->save();

            // requires renewal before setting old calibration status to expired
            $this->equipmentCalibrationStatusLogic->setStatus($equipment_calibration, EquipmentCalibration::STATUS_EXPIRED);
        }
    }

    public function update(EquipmentCalibration $equipment_calibration, Request $request)
    {
        if ($file = $request->file('file'))
        {
            $equipment_calibration->document_path = DocumentHandler::update($equipment_calibration->document_path, $equipment_calibration->document_store_path, $file);
        }

        $equipment_calibration->update($request->all());
    }

    public function delete($equipment_calibration)
    {
        DocumentHandler::delete($equipment_calibration->document_path);
        $equipment_calibration->delete();
    }
}
